#!/bin/bash

mv -f /home/kubekwp/Downloads/d_pl_txt.zip .
rm -rf data
unzip -q d_pl_txt.zip
rm -rf indicators
rm -rf stocks
mkdir indicators
mkdir stocks
cp -r data/daily/pl/wse\ stocks/* stocks
cp -r data/daily/pl/wse\ stocks\ indicators/* indicators
