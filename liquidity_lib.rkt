#lang racket
(require "common.rkt")
(require "names_lib.rkt")

(define (turnover rec)
  (* (close-price rec) (volume rec)))

(define (last-n-records code n)
  (map (file-line-to-record code) (take-right (file->lines (filename code)) n)))


(define (avg-turnover code n position-size)
  (let ((avgt (avg 
               (map turnover (last-n-records code n)))))
    (list code avgt (round-off (/ avgt position-size) 2.0))))


(define (avg-turnover-all n position-size)
  (map (lambda (code) (avg-turnover code n position-size)) (codes-all)))


(define (period args)
  (if (= (vector-length args) 2)
      (string->number (vector-ref args 1))
      7))

(provide avg-turnover avg-turnover-all)
