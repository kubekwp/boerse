import sun.security.ssl.HandshakeInStream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class YearSessions {
    private String first;
    private String last;
    private final Map<String, List<String[]>> yearDates;

    public YearSessions(int startYear, int endYear) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get("..\\..\\gpw-sessions.txt"));
        yearDates = lines.stream()
                .map(line -> line.split(" "))
                .collect(Collectors.groupingBy(o -> o[0]));
    }

    public String getFirst(int year) {
        return yearDates.get(String.valueOf(year)).get(0)[1];
    }

    public String getLast(int year) {
        return yearDates.get(String.valueOf(year)).get(0)[2];
    }

    public static void main(String[] args) throws IOException {
        YearSessions ys = new YearSessions(2005, 2016);
        IntStream.rangeClosed(2005,2016)
                .forEach(year -> System.out.println(ys.getFirst(year) + " " + ys.getLast(year)));
    }
}