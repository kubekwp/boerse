import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Sessions {

    public static void main(String[] args) throws IOException {
        List<String> lines = IntStream.rangeClosed(2005, 2016)
                .mapToObj(year -> String.format("%s %s-01-01 %s-12-31", year, year, year))
                .collect(Collectors.toList());

        Files.write(Paths.get("gpw-sessions.txt"), lines, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }
}