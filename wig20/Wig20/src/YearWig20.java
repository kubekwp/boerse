import java.util.List;

public class YearWig20 {
    private int year;
    private List<String> composition;

    public YearWig20(int year, List<String> composition) {
        this.year = year;
        this.composition = composition;
    }

    public int getYear() {
        return year;
    }

    public List<String> getComposition() {
        return composition;
    }
}