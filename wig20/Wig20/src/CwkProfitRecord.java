import java.text.DecimalFormat;

public class CwkProfitRecord {
    private String name;
    private String code;
    private String startDate;
    private String endDate;
    private double cwk;
    private double startPrice;
    private double endPrice;

    private CwkProfitRecord(String name, String code, String startDate, String endDate, double cwk, double startPrice, double endPrice) {
        this.name = name;
        this.code = code;
        this.startDate = startDate;
        this.endDate = endDate;
        this.cwk = cwk;
        this.startPrice = startPrice;
        this.endPrice = endPrice;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public double getCwk() {
        return cwk;
    }

    public double getStartPrice() {
        return startPrice;
    }

    public double getEndPrice() {
        return endPrice;
    }

//    public double getProfit() {
//        return profit;
//    }

    public static class CwkProfitRecordBuilder {
        private String name;
        private String code;
        private String startDate;
        private String endDate;
        private double cwk;
        private double startPrice;
        private double endPrice;

        public CwkProfitRecordBuilder(String name) {
            this.name = name;
        }

        public CwkProfitRecord build() {
            return new CwkProfitRecord(name, code, startDate, endDate, cwk, startPrice, endPrice);
        }

        public CwkProfitRecordBuilder withCode(String code) {
            this.code = code;
            return this;
        }

        public CwkProfitRecordBuilder withStartDate(String startDate) {
            this.startDate = startDate;
            return this;
        }

        public CwkProfitRecordBuilder withEndDate(String endDate) {
            this.endDate = endDate;
            return this;
        }

        public CwkProfitRecordBuilder withCwk(double cwk) {
            this.cwk = cwk;
            return this;
        }

        public CwkProfitRecordBuilder withStartPrice(double startPrice) {
            this.startPrice = startPrice;
            return this;
        }

        public CwkProfitRecordBuilder withEndPrice(double endPrice) {
            this.endPrice = endPrice;
            return this;
        }
    }

    @Override
    public String toString() {
        return String.format("%s | %s | sd=%s" +
                        ", ed=" + endDate +
                        ", cwk=" + cwk +
                        ", sp=" + startPrice +
                        ", ep=" + endPrice +
                        ", p=" + new DecimalFormat("0.0").format(calcProfit()),
                name, code, startDate);
    }

    public double calcProfit() {
        return (endPrice - startPrice) / startPrice * 100;
    }
}
