import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.List;
import java.util.function.IntFunction;
import java.util.stream.IntStream;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

public class WigCwk {

    private final static String BASE_DIR = "E:\\Borse";

    public static void main(String[] args) throws IOException {
        IntFunction<YearWig20> yearToWig20 = year -> {
            try {
                return new YearWig20(year, Files.readAllLines(Paths.get(String.format("%s\\wig20\\%s.txt", BASE_DIR, year))));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        };

        int startYear = 2008;
        int endYear = 2016;

        List<YearWig20> yearWigList = IntStream.rangeClosed(startYear, endYear)
                .mapToObj(yearToWig20)
                .collect(toList());

        YearSessions yearSessions = new YearSessions(startYear, endYear);

        List<CwkProfitWigRecord> records = yearWigList.stream()
                .map(yearWig -> new CwkProfitWigRecord(yearWig.getYear(), yearWig.getComposition().stream()
                        .filter(name -> !"NA".equals(nameToCode(name)))
                        .map(name -> new CwkProfitRecord.CwkProfitRecordBuilder(name)
                                .withCode(nameToCode(name))
                                .withStartDate(yearSessions.getFirst(yearWig.getYear()))
                                .withEndDate(yearSessions.getLast(yearWig.getYear()))
                                .withCwk(cwkFor(nameToCode(name), yearSessions.getFirst(yearWig.getYear())))
                                .withStartPrice(priceFor(nameToCode(name), yearSessions.getFirst(yearWig.getYear())))
                                .withEndPrice(priceFor(nameToCode(name), yearSessions.getLast(yearWig.getYear())))
                                .build())
                        .sorted(comparing(CwkProfitRecord::getCwk))
                        .collect(toList())))
                .collect(toList());

        records.stream()
                .forEach(record -> System.out.println(
                        String.format("y= %s | wp= %s | lcwkp= %s | hcwkp= %s",
                                record.getYear(),
                                doubleFormat(record.calcWigProfit()),
                                doubleFormat(record.calcLowestCwkProfit(3)),
                                doubleFormat(record.calcHighestCwkProfit(3)))));
    }

    private static String doubleFormat(double value) {
        return new DecimalFormat("0.0").format(value);
    }

    private static double priceFor(String code, String session) {
        try {
            return Double.valueOf(Files.readAllLines(Paths.get(getPriceFileName(code))).stream()
                    .filter(line -> line.startsWith(session))
                    .findFirst().orElse("NO,0.0,0.0,0.0,-9999.0").split(",")[4]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -9999.0;
    }

    public static double cwkFor(String code, String session) {
        try {
            return Double.valueOf(Files.readAllLines(Paths.get(getCwkFileName(code))).stream()
                    .filter(line -> line.startsWith(session))
                    .findFirst().orElse("NO,0.0,0.0,0.0,9999.0").split(",")[4]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 9999.0;
    }

    private static String getCwkFileName(String code) {
        return Files.exists(Paths.get(String.format("%s\\indicators\\%s_pb.txt", BASE_DIR, code)))
                ? String.format("%s\\indicators\\%s_pb.txt", BASE_DIR, code)
                : String.format("%s\\indicators_hist\\%s_pb.txt", BASE_DIR, code);
    }

    private static String getPriceFileName(String code) {
        return Files.exists(Paths.get(String.format("%s\\stocks\\%s.txt", BASE_DIR, code)))
                ? String.format("%s\\stocks\\%s.txt", BASE_DIR, code)
                : String.format("%s\\stocks_hist\\%s.txt", BASE_DIR, code);
    }

    public static String nameToCode(String name) {
        try {
            return Files.readAllLines(Paths.get("E:\\Borse\\gpw-codes.txt")).stream()
                    .filter(line -> line.startsWith(name))
                    .findFirst().orElse(null).split(" : ")[1];
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

//    public static void main(String[] args) {
//        System.out.println(cwkFor("PKO", "20070822"));
//    }
}