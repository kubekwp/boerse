import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Wig20 {

    public static void main(String[] args) throws IOException {
        try {
            List<String> lines = Files.readAllLines(Paths.get("WIG20 sklad.txt")).stream()
                    .filter(s -> !s.isEmpty())
                    .collect(Collectors.toList());

            IntStream.rangeClosed(2005, 2017)
                    .mapToObj(new YearLines(lines))
                    .forEach(yc -> {
                        try {
                            String fileName = String.format("%s.txt", yc.getYear());
                            Files.write(Files.createFile(Paths.get(fileName)), yc.getComposition());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class YearLines implements IntFunction<YearComposition> {

    private List<String> lines;

    public YearLines(List<String> lines) {
        this.lines = lines;
    }

    @Override
    public YearComposition apply(int year) {
        List<String> result = new ArrayList<>();
        boolean on = false;
        for (String line : lines) {
            if (line.contains(String.format("%s:", year))) {
                on = true;
            } else if (line.contains(String.format("%s:", year + 1))) {
                on = false;
            } else if (on) {
                result.add(line);
            }
        }

        return new YearComposition(year,
                result.stream()
                        .map(line -> line.length() > 19
                                ? new Scanner(line.substring(15).trim()).next()
                                : line.substring(2).trim())
                        .collect(Collectors.toList()));
    }
}

class YearComposition {
    private int year;
    private List<String> composition;

    public YearComposition(int year, List<String> composition) {
        this.year = year;
        this.composition = composition;
    }

    public int getYear() {
        return year;
    }

    public List<String> getComposition() {
        return composition;
    }
}