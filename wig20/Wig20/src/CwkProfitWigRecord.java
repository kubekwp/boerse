import java.util.Comparator;
import java.util.List;

public class CwkProfitWigRecord {
    private int year;
    private List<CwkProfitRecord> records;

    public CwkProfitWigRecord(int year, List<CwkProfitRecord> records) {
        this.year = year;
        this.records = records;
    }

    public int getYear() {
        return year;
    }

    public List<CwkProfitRecord> getRecords() {
        return records;
    }

    public double calcWigProfit() {
        return records.stream()
                .mapToDouble(CwkProfitRecord::calcProfit)
                .average()
                .getAsDouble();
    }

    public double calcLowestCwkProfit(int num) {
        return records.stream()
                .sorted(Comparator.comparing(CwkProfitRecord::getCwk))
                .limit(num)
                .mapToDouble(CwkProfitRecord::calcProfit)
                .average()
                .getAsDouble();
    }

    public double calcHighestCwkProfit(int num) {
        return records.stream()
                .sorted(Comparator.comparing(CwkProfitRecord::getCwk))
                .skip(records.size() - num)
                .mapToDouble(CwkProfitRecord::calcProfit)
                .average()
                .getAsDouble();
    }
}
